package clases;

import java.time.LocalDate;

public class Cliente {
	
	private String nombre;
	private String dni;
	private LocalDate fecha;
	

	public Cliente(String nombre,String dni,LocalDate fecha){
		this.nombre=nombre;
		this.dni=dni;
		this.fecha=fecha;
	}
	public Cliente(){
		this.nombre="";
		this.dni="";
		this.fecha=null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	/*rellenarCliente(){
		
	}
	*/
	
}
