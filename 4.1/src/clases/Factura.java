package clases;

import java.time.LocalDate;

public class Factura {

	private String codigoFactura;
	private LocalDate fecha;
	private String nombreProducto;
	private int precioUnidad;
	private int cantidad; 
	private Cliente unCliente;
	
	public Factura(String codigo,LocalDate fecha,String producto,int precioUnidad,int cantidad,Cliente c){
		this.codigoFactura=codigo;
		this.fecha=fecha;
		this.nombreProducto=producto;
		this.precioUnidad=precioUnidad;
		this.cantidad=cantidad;
		this.unCliente=c;
	}

	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(int precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getUnCliente() {
		return unCliente;
	}

	public void setUnCliente(Cliente unCliente) {
		this.unCliente = unCliente;
	}
}
