package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	private  ArrayList <Cliente>listaClientes ;
	private ArrayList <Factura>listaFacturas;
	
	public GestorContabilidad(){
		listaClientes = new ArrayList <Cliente>();
		listaFacturas = new ArrayList <Factura>();
	}

	public Cliente buscarCliente(String dni){
		for (Cliente unCliente : listaClientes) {
			if(dni.equalsIgnoreCase(unCliente.getDni())){
				return unCliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		for (Factura unaFactura : listaFacturas) {
			if(codigo.equalsIgnoreCase(unaFactura.getCodigoFactura())){
				return unaFactura;
			}
		}
		return null;
	}
	
	/*
	 * void altaCliente(Cliente cliente): recibe un objeto de tipo Cliente y lo a�ade a la lista de
clientes. Este m�todo debe comprobar que el cliente que a�adimos no tiene un dni que ya
tenemos en la lista. Si el dni ya existe no a�adir� al cliente.
	 * */
	
	public void altaCliente(Cliente cliente){
		boolean Existe=false;
		for (Cliente unCliente : listaClientes) {
			if(cliente.getDni().equalsIgnoreCase(unCliente.getDni())){
				Existe=true;
			}
		}
		if(!Existe){
			listaClientes.add(cliente);
		}else{
			System.out.println("El cliente ya existe en la base de datos");
		}
	}
	
	public void crearFactura(Factura fectura){
		boolean Existe=false;
		for (Factura unaFactura : listaFacturas) {
			if(fectura.getCodigoFactura()==unaFactura.getCodigoFactura()){
				Existe=true;
			}
		}
		if(Existe){
			listaFacturas.add(fectura);
		}else{
			System.out.println("El cliente ya existe en la base de datos");
		}
	}
	
	public Cliente clienteMasAntiguo(){
		return null;
	}
	
	public Factura facturaMasCara(){
		String codigoFactura=""; 
		int valorFactura=0;
		int mayorValor=0;
			for (Factura unaFactura : listaFacturas) {
				valorFactura=unaFactura.getPrecioUnidad()*unaFactura.getCantidad();
				if(mayorValor<valorFactura){
					mayorValor=valorFactura;
					codigoFactura=unaFactura.getCodigoFactura();
				}
			}
			for(Factura unaFactura : listaFacturas){
				if(codigoFactura.equalsIgnoreCase(unaFactura.getCodigoFactura())){
					return unaFactura;
				}
			}
		return null;
	}
	
	public float calculaFacturacionAnual(int anno){
		float facturacionAnual=0;
		for (Factura unaFactura : listaFacturas) {
			
			if(anno==0){
				
			}
		}
		return facturacionAnual;
	}
	
	public int cantidadFacturasPorCliente(String dni){
		
		return 0;
	}
	
	public void eliminarFactura(String codigo){
		for (Factura unaFactura : listaFacturas) {
			if(codigo.equalsIgnoreCase(unaFactura.getCodigoFactura())){
				
			}
		}
	}
	
	public void eliminarCliente(String dni){
		
	}
	
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}



	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}


	
}
