package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class PruebasFactura {
	private static GestorContabilidad gestorPruebas;
	private static Factura actual;
	private static Factura esperado;
	
	@BeforeClass
	public static void elementosAntesdeLaClase() throws Exception{
		gestorPruebas = new GestorContabilidad();
		Cliente unCliente = new Cliente ("fernando","5252525",LocalDate.parse("1-1-2015"));
		esperado = new Factura("12",LocalDate.parse("12-2-1900"),"fairi",30,2,unCliente);
		gestorPruebas.getListaFacturas().add(esperado);
	}
	
	@Test
	public void buscarFacturaInexistente(){
		String codigo ="13";
		actual=gestorPruebas.buscarFactura(codigo);
		assertNull(actual);
	}
	
	@Test 
	public void buscarFacturaExistente(){
		String codigo = "12"; 
		actual = gestorPruebas.buscarFactura(codigo);
		assertEquals(actual,esperado);
	}
	
	@Test
	public void facturaMasCaraInexistente(){
		actual=gestorPruebas.facturaMasCara();
		assertNull(actual);
	}
	
	@Test 
	public void facturaMasCaraConFacturas(){
		Cliente cliente1 = new Cliente("juan","45644848g",LocalDate.parse("20-4-1808"));
		Cliente cliente2 = new Cliente("pedro","45454548c",LocalDate.parse("18-5-1900"));
		Cliente cliente3 = new Cliente("edu","77777777f",LocalDate.parse("17-6-1200"));
		Factura factura1= new Factura("12",LocalDate.parse("12-2-1900"),"fairi",1,1,cliente1);
		Factura factura2= new Factura("12",LocalDate.parse("12-2-1900"),"fairi",2,2,cliente2);
		esperado = new Factura("12",LocalDate.parse("12-2-1900"),"fairi",30,30,cliente1);
		gestorPruebas.getListaFacturas().add(factura1);
		gestorPruebas.getListaFacturas().add(factura2);
		gestorPruebas.getListaFacturas().add(esperado);
		actual=gestorPruebas.facturaMasCara();
		assertEquals(esperado,actual);
	}
	
	@Test 
	public void elminarFactura(){
		Cliente cliente1 = new Cliente("carlos","45644848g",LocalDate.parse("20-4-1808"));
		Factura factura1= new Factura("12",LocalDate.parse("12-2-1900"),"fairi",1,1,cliente1);
		gestorPruebas.getListaFacturas().add(factura1);
		gestorPruebas.eliminarFactura(factura1.getCodigoFactura());
		actual=gestorPruebas.buscarFactura(factura1.getCodigoFactura());
		assertNull(actual);		
	}

}
