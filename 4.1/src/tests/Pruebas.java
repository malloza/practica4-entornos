package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;
import clases.Cliente;
import clases.GestorContabilidad;

public class Pruebas {
	
	private static GestorContabilidad gestorPruebas;
	private static Cliente actual;
	private static Cliente esperado;
	
	@BeforeClass
	public static void elementosAntesdeLaClase() throws Exception{
		gestorPruebas = new GestorContabilidad();
		actual=new Cliente();
		esperado = new Cliente("manuel","73222988g",LocalDate.parse("11-8-1996"));
		gestorPruebas.getListaClientes().add(esperado);
	}
	
	@Test 
	public void buscarClienteInexistente(){
		String dni = "76658454g";
		actual=gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	
	@Test 
	public void buscarClienteExistente(){
		String dni = "73222988g";
		actual=gestorPruebas.buscarCliente(dni);
		assertSame(actual,esperado);
	}
	
	@Test 
	public void clienteMasAntiguoSinClientes(){
		actual=gestorPruebas.clienteMasAntiguo();
		assertNull(actual);
	}
	
	@Test 
	public void testBuscarClienteMasAntiguoConClientes(){
		Cliente cliente1 = new Cliente("juan","45644848g",LocalDate.parse("20-4-1808"));
		Cliente cliente2 = new Cliente("pedro","45454548c",LocalDate.parse("18-5-1900"));
		esperado = new Cliente("edu","77777777f",LocalDate.parse("17-6-1200"));
		gestorPruebas.getListaClientes().add(cliente1);
		gestorPruebas.getListaClientes().add(cliente2);
		gestorPruebas.getListaClientes().add(esperado);
		actual=gestorPruebas.clienteMasAntiguo();
		assertEquals(actual,esperado);	
	}
	
	@Test 
	public void testElminarCliente(){
		String dni="4584548g";
		gestorPruebas.eliminarCliente(dni);
		actual=gestorPruebas.buscarCliente(dni);
		assertNull(actual);		
	}
	 
	@Test 
	public void ComprobarClienteExiste(){
		boolean bien = false;
		String dni ="45644848g";
		int cont = 0;
		Cliente cliente1 = new Cliente("juan","45644848g",LocalDate.parse("20-4-1808"));
		Cliente cliente2 = new Cliente("juan","45644848g",LocalDate.parse("20-4-1808"));
		gestorPruebas.altaCliente(cliente1);
		gestorPruebas.altaCliente(cliente2);
		for (Cliente unCliente : gestorPruebas.getListaClientes()) {
			if(unCliente.getDni().equalsIgnoreCase(dni)){
				cont++;
			}
		}
		if(cont==1){
			bien=true;
		}
		assertTrue(bien);
	}
	
}
